import math


# первая заадча
def uniq_num(num):
    return list(set(num))


# вторая задача
def primes_in_range(minimum, maximum):
    if minimum >= maximum:
        return []
    primes = [number for number in range(minimum, maximum + 1) if is_prime(number)]
    return primes


def is_prime(num):
    if num <= 1:
        return False
    elif num == 2:
        return True
    else:
        for i in range(2, int(math.pow(num, 0.5)) + 1):
            if num % i == 0:
                return False
        return True


# третья задача
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def distance_to(self, point) -> int | float:
        if not isinstance(point, Point):
            raise ValueError("Невозможно вычислить расстояние между точкой и другим объектом")
        return math.pow((math.pow((self.x - point.x), 2) + pow((self.y - point.y), 2)), 0.5)

    @property
    def x(self) -> int | float:
        return self._x

    @property
    def y(self) -> int | float:
        return self._y

    @x.setter
    def x(self, value):
        if not isinstance(value, (int, float)):
            raise ValueError("Неверное значение абсциссы")
        self._x = value

    @y.setter
    def y(self, value):
        if not isinstance(value, (int, float)):
            raise ValueError("Неверное значение ординаты")
        self._y = value


# четвертая задача
def sort_strings_by_length(str_list: list) -> list:
    if not isinstance(str_list, list):
        raise ValueError("Невозможно отсортировать не список")
    sorted_str_list = sorted(str_list, key=len)
    sorted_str_list = sorted(sorted_str_list, key=len, reverse=True)
    return sorted_str_list
